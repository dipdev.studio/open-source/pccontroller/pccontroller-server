import 'dart:convert';
import 'dart:ffi';
import 'dart:io';

import 'package:ffi/ffi.dart';
import 'package:win32/win32.dart';

void main(List<String> arguments) {
  bind();
}

void sendInput(int wVk) {
  if (Platform.isWindows) {
    final kbd = calloc<INPUT>();
    kbd.ref.type = INPUT_KEYBOARD;
    kbd.ki.wVk = wVk;
    var result = SendInput(1, kbd, sizeOf<INPUT>());
    if (result != TRUE) print('Error: ${GetLastError()}');
  }
}

void bind() {
  RawDatagramSocket.bind(InternetAddress.anyIPv4, 7660)
      .then((RawDatagramSocket udpSocket) {
    print('Server binding brodcast started!');
    udpSocket.broadcastEnabled = true;
    udpSocket.listen((e) {
      switch (e) {
        case RawSocketEvent.read:
          final dg = udpSocket.receive();
          utf8.decode(dg.data);
          if (dg != null) {
            parseRequest(utf8.decode(dg.data));
          }
          break;
        case RawSocketEvent.write:
          break;
        case RawSocketEvent.closed:
          print('Client disconnected.');
      }
    });
  });
}

void parseRequest(String input) {
  switch (input) {
    case 'volume_up':
      sendInput(VK_VOLUME_UP);
      break;
    case 'volume_down':
      sendInput(VK_VOLUME_DOWN);
      break;
    case 'volume_mute':
      sendInput(VK_VOLUME_MUTE);
      break;
    case 'forward':
      sendInput(VK_RIGHT);
      break;
    case 'rewind':
      sendInput(VK_LEFT);
      break;
    case 'play_pause':
      sendInput(VK_MEDIA_PLAY_PAUSE);
      break;
    default:
  }
}
